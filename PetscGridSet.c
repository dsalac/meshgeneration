/*	This creates a grid HDF file*/

#define PROGRAM_GRID_FILE_VERSION_MAJOR 2
#define PROGRAM_GRID_FILE_VERSION_MINOR 0
#define STANDARD 0
#define OUTFLOW 1
#define PERIODIC 2

#include <petsc.h>
#include <petscdmforest.h>
#include <unistd.h>
#include "shapes.h"
#include <petscviewerhdf5.h>

#define FALSE 0
#define TRUE 1

#define EPS 1E-12

struct _shape_funcs_struct{
  double         (**shape_func)(const double,  const double,  const double,  const int,  const int,  const int,  const int,  void *);
  void           **ctx;
  PetscBool		   *isDomain;
  PetscInt       n; /* Number of functions */
};

typedef struct _shape_funcs_struct ShapeFunctions;

struct _idx_lists_ctx_struct{
  PetscInt     szLocal, szGhost;
  PetscInt     *localIdx, *ghostIdx;
};

typedef struct _idx_lists_ctx_struct ISCtx;

void CreateShapeFunctionStructure(ShapeFunctions *SF)
{

  SF->n = 0;
  SF->shape_func = NULL;
  SF->ctx = NULL;
  SF->isDomain = NULL;
}

void DestroyShapeFunctionStructure(ShapeFunctions *SF)
{

  for(int i=0;i<SF->n;++i){
    PetscFree(SF->ctx);
  }

  SF->n = 0;
  SF->shape_func = NULL;
  SF->ctx = NULL;
}

static PetscErrorCode AddShapeFunction(ShapeFunctions *SF,
		                                   double (*func)(const double,  const double,  const double,  const int, const int,  const int,  const int,  void *),
		                                   PetscBool isDomain,
		                                   void *ctx)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if 1
  if (!SF->n) {
    ierr = PetscMalloc1(SF->n+1, &SF->shape_func);CHKERRQ(ierr);
    ierr = PetscMalloc1(SF->n+1, &SF->ctx);CHKERRQ(ierr);
    ierr = PetscMalloc1(SF->n+1, &SF->isDomain);CHKERRQ(ierr);
  } else {
#endif
    ierr = PetscRealloc(sizeof(double*)*(SF->n+1), &SF->shape_func);CHKERRQ(ierr);
    ierr = PetscRealloc(sizeof(void*)*(SF->n+1), &SF->ctx);CHKERRQ(ierr);
    ierr = PetscRealloc(sizeof(PetscBool*)*(SF->n+1), &SF->isDomain);CHKERRQ(ierr);
#if 1
  }
#endif

  SF->shape_func[SF->n] = func;
  SF->ctx[SF->n] = ctx;
  SF->isDomain[SF->n] = isDomain;
  ++SF->n;
  PetscFunctionReturn(0);
}

double GetDistance(const double x, const double y, const double z, const int dim,
			  double (*shape_func)(const double, const double, const double, const int, const int, const int, const int, void *),
			  void *ctx){

  double c = PetscAbsReal(shape_func(x, y, z, 0, 0, 0, dim, ctx));
  double cx = PetscAbsReal(shape_func(x, y, z, 1, 0, 0, dim, ctx));
  double cy = PetscAbsReal(shape_func(x, y, z, 0, 1, 0, dim, ctx));
  double cz = PetscAbsReal(shape_func(x, y, z, 0, 0, 1, dim, ctx));

  return(c/(sqrt(cx*cx+cy*cy+cz*cz)+1e-8));
}

PetscErrorCode RefineDomain(PetscInt c, DM cdm, Vec coordinates, PetscInt *label_val, ShapeFunctions SF)
{

  PetscErrorCode ierr;
  PetscInt       dim;
  ierr = DMGetDimension(cdm,&dim);CHKERRQ(ierr);  // First get the dimension (2 or 3) of the DM

  PetscInt 	    nPoints = dim==2?4:8;	// Number of points in the cell
  PetscInt      csize;			// Number of coordinates. Should be dim*nPoints
  PetscScalar   *coords = NULL;		// Actual coordinates
  PetscReal     x[nPoints], y[nPoints], z[nPoints];
  PetscReal	    S;			// Level set value
  PetscInt	    i, j;			// Counters

  /* Get Coordinates */
  ierr = DMPlexVecGetClosure(cdm, NULL, coordinates, c, &csize, &coords);CHKERRQ(ierr);

  // Check csize
  if(csize!=nPoints*dim){
    printf("Mismatch in number of returned points.\n");
  }

  // Parse the coordinates
  if (dim == 2) {
    for(i=0;i<nPoints;++i){
      x[i] = PetscRealPart(coords[i*dim+0]);
      y[i] = PetscRealPart(coords[i*dim+1]);
      z[i] = 0.0;
    }
  }
  else{
    for(i=0;i<nPoints;++i){
      x[i] = PetscRealPart(coords[i*dim+0]);
      y[i] = PetscRealPart(coords[i*dim+1]);
      z[i] = PetscRealPart(coords[i*dim+2]);
    }
  }

  *label_val = DM_ADAPT_KEEP;
  for(i=0;(i<nPoints && *label_val==DM_ADAPT_KEEP);++i){			// Iterate over every cell corner
    for(j=0;(j<SF.n && *label_val==DM_ADAPT_KEEP);++j){				// Iterate over each shape function
      if(SF.isDomain[j]){							// Only worry about domain shapes
	S = SF.shape_func[j](x[i], y[i], z[i], 0, 0, 0, dim, SF.ctx[j]);	// Get the SDF
	if(S<=0.0){								// Refine if any corner is inside the domain
	  *label_val = DM_ADAPT_REFINE;
	}
      }
    }
  }

  ierr = DMPlexVecRestoreClosure(cdm, NULL, coordinates, c, &csize, &coords);CHKERRQ(ierr);
  return(0);
}


static PetscErrorCode CoarsenDomain(PetscInt c, DM cdm, Vec coordinates, PetscInt *label_val, ShapeFunctions SF)
{

  PetscErrorCode ierr;
  PetscInt       dim;
  ierr = DMGetDimension(cdm,&dim);CHKERRQ(ierr);  // First get the dimension (2 or 3) of the DM

  PetscInt 	    nPoints = dim==2?4:8;	// Number of points in the cell
  PetscInt      csize;			// Number of coordinates. Should be dim*nPoints
  PetscScalar   *coords = NULL;		// Actual coordinates
  PetscReal     x[nPoints], y[nPoints], z[nPoints];
  PetscReal	    S;			// Level set value
  PetscInt	    i, j;			// Counters

  /* Get Coordinates */
  ierr = DMPlexVecGetClosure(cdm, NULL, coordinates, c, &csize, &coords);CHKERRQ(ierr);

  // Check csize
  if(csize!=nPoints*dim){
    printf("Mismatch in number of returned points.\n");
  }

  // Parse the coordinates
  if (dim == 2) {
    for(i=0;i<nPoints;++i){
      x[i] = PetscRealPart(coords[i*dim+0]);
      y[i] = PetscRealPart(coords[i*dim+1]);
      z[i] = 0.0;
    }
  }
  else{
    for(i=0;i<nPoints;++i){
      x[i] = PetscRealPart(coords[i*dim+0]);
      y[i] = PetscRealPart(coords[i*dim+1]);
      z[i] = PetscRealPart(coords[i*dim+2]);
    }
  }

  *label_val = DM_ADAPT_COARSEN;
  for(i=0;(i<nPoints && *label_val==DM_ADAPT_COARSEN);++i){			// Iterate over every cell corner
    for(j=0;(j<SF.n && *label_val==DM_ADAPT_COARSEN);++j){				// Iterate over each shape function
      if(SF.isDomain[j]){							// Only worry about domain shapes
	S = SF.shape_func[j](x[i], y[i], z[i], 0, 0, 0, dim, SF.ctx[j]);	// Get the SDF
	if(S<=0.0){								// Refine if any corner is inside the domain
	  *label_val = DM_ADAPT_KEEP;
	}
      }
    }
  }

  ierr = DMPlexVecRestoreClosure(cdm, NULL, coordinates, c, &csize, &coords);CHKERRQ(ierr);
  return(0);
}

static PetscErrorCode RefineInterface(PetscInt c, DM cdm, Vec coordinates, PetscInt *label_val, ShapeFunctions SF)
{

  PetscErrorCode ierr;
  PetscInt       dim;
  ierr = DMGetDimension(cdm,&dim);CHKERRQ(ierr);  // First get the dimension (2 or 3) of the DM

  PetscInt 	    nPoints = dim==2?4:8;	// Number of points in the cell
  PetscInt      csize;			// Number of coordinates. Should be dim*nPoints
  PetscScalar   *coords = NULL;		// Actual coordinates
  PetscReal     x[nPoints], y[nPoints], z[nPoints];
  PetscReal	    S;			// Level set value
  PetscReal	    diag;			// Cell diagonal
  PetscInt	    i, j;			// Counters

  /* Get Coordinates */
  ierr = DMPlexVecGetClosure(cdm, NULL, coordinates, c, &csize, &coords);CHKERRQ(ierr);

  // Check csize
  if(csize!=nPoints*dim){
    printf("Mismatch in number of returned points.\n");
  }

  // Parse the coordinates
  if (dim == 2) {
    for(i=0;i<nPoints;++i){
      x[i] = PetscRealPart(coords[i*dim+0]);
      y[i] = PetscRealPart(coords[i*dim+1]);
      z[i] = 0.0;
    }
    diag = sqrt(PetscSqr(x[3] - x[1]) + PetscSqr(y[3] - y[1]));
  }
  else{
    for(i=0;i<nPoints;++i){
      x[i] = PetscRealPart(coords[i*dim+0]);
      y[i] = PetscRealPart(coords[i*dim+1]);
      z[i] = PetscRealPart(coords[i*dim+2]);
    }
    diag = sqrt(PetscSqr(x[7] - x[1]) + PetscSqr(y[7] - y[1]) + PetscSqr(z[7] - z[1]));
  }

  // First do all embedded interfaces
  *label_val = DM_ADAPT_KEEP;
  for(i=0;(i<nPoints && *label_val==DM_ADAPT_KEEP);++i){			// Iterate over every cell corner
    // First get the embedding domain level set value.
    S = 1E8;
    for(j=0;(j<SF.n);++j){
      if(SF.isDomain[j]){
	S = PetscMin(S, SF.shape_func[j](x[i], y[i], z[i], 0, 0, 0, dim, SF.ctx[j]));
      }
    }

    if(fabs(S)<=diag){
      *label_val = DM_ADAPT_REFINE;
    }
    else if(S<=0.0){      							// Only check if actually in the domain.
      for(j=0;(j<SF.n && *label_val==DM_ADAPT_KEEP);++j){			// Iterate over each shape function
	if(!SF.isDomain[j]){
	  S = SF.shape_func[j](x[i], y[i], z[i], 0, 0, 0, dim, SF.ctx[j]);	// Get the SDF
	  if(fabs(S)<=diag){
	    *label_val = DM_ADAPT_REFINE;
	  }
	}
      }
    }
  }

  ierr = DMPlexVecRestoreClosure(cdm, NULL, coordinates, c, &csize, &coords);CHKERRQ(ierr);
  return(0);
}

static PetscErrorCode AdaptGrid(DM preforest, PetscErrorCode (*func)(PetscInt, DM, Vec, PetscInt *, ShapeFunctions), ShapeFunctions SF, DM *postforest)
{
  DM             cdm;
  DMLabel 	     adaptLabel = NULL;
  Vec            coordinates;
  PetscInt       cStart, cEnd, c, label_val;
  PetscErrorCode ierr;


  ierr = DMForestGetCellChart(preforest, &cStart, &cEnd);CHKERRQ(ierr);
  ierr = DMGetCoordinateDM(preforest, &cdm);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(preforest, &coordinates);CHKERRQ(ierr);
  ierr = DMLabelCreate(PETSC_COMM_SELF, "adapt", &adaptLabel);CHKERRQ(ierr);
  for (c = cStart; c < cEnd; ++c) {
    ierr = func(c, cdm, coordinates, &label_val, SF);CHKERRQ(ierr);
    ierr = DMLabelSetValue(adaptLabel, c, label_val);CHKERRQ(ierr);
  }
  ierr = DMForestTemplate(preforest, PETSC_COMM_WORLD, postforest);CHKERRQ(ierr);
  ierr = DMForestSetAdaptivityLabel(*postforest, adaptLabel);CHKERRQ(ierr);
  ierr = DMLabelDestroy(&adaptLabel);CHKERRQ(ierr);
  ierr = DMSetUp(*postforest);CHKERRQ(ierr);
  return(0);
}

void parse_file(char *InputFile, char *fname, int *min_refine, int *max_refine, int *dim, double xDomain[2], double yDomain[2], double zDomain[2],
    int *bType, int *showData, int *nDomains, ShapeFunctions *SF){
    char chtmp[255];

    if(access(InputFile, R_OK)){
      printf("File %s is not available for read access.\n", InputFile);
      exit(0);
    }

    FILE *f1 = fopen(InputFile,"r");

    *nDomains = 0;

    /*****************************************************************/
    /*********Read in the required values from the input file*********/
    /*****************************************************************/
    while(!feof(f1)){

      if(fscanf(f1,"%s", chtmp)>0){


	if(!strcmp(chtmp,"grid_file")){	/*	File name to save to*/
	  if(fscanf(f1, "%s", fname)<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp(chtmp, "refinements")){	/*	Number of grid points*/
	  if(fscanf(f1, "%d %d", min_refine, max_refine)<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp(chtmp, "dimensions")){	/*	Number of grid points*/
	  if(fscanf(f1, "%d", dim)<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp(chtmp, "domain_size")){
	  if(fscanf(f1, "%lf %lf", &xDomain[0], &xDomain[1])<0){ printf("Error in fscanf.\n");}
	  if(fscanf(f1, "%lf %lf", &yDomain[0], &yDomain[1])<0){ printf("Error in fscanf.\n");}
	  if(fscanf(f1, "%lf %lf", &zDomain[0], &zDomain[1])<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp(chtmp, "shape") || !strcmp(chtmp,"domain")){

	  PetscBool isDomain = !strcmp(chtmp,"domain");
	  *nDomains += (int)isDomain;

	  if(fscanf(f1,"%s",chtmp)<0){ printf("Error in fscanf.\n");}

	  if(!strcmp("sphere", chtmp)){
	    struct sphere_data *circle;
      PetscNew(&circle);
	    if(fscanf(f1,"%lf %lf %lf %lf", &(circle->R), &(circle->xc), &(circle->yc), &(circle->zc))<0){ printf("Error in fscanf.\n");}
	    AddShapeFunction(SF, &sphere, isDomain, (void*)circle);
	  }
	  else if(!strcmp("hollow_sphere", chtmp)){
	    struct hollow_sphere_data *r;
      PetscNew(&r);
	    if(fscanf(f1, "%lf %lf %lf %lf %lf", &(r->r0), &(r->r1), &(r->xc), &(r->yc), &(r->zc))<0){ printf("Error in fscanf.\n");}
	    if(r->r1<r->r0){
	      double t = r->r0;
	      r->r0 = r->r1;
	      r->r1 = t;
	    }
	    AddShapeFunction(SF, &hollow_sphere, isDomain, (void*)r);
	  }
	  else if(!strcmp("cylinder", chtmp)){
	    struct cylinder_data *data;
      PetscNew(&data);
	    if(fscanf(f1, "%d %lf", &(data->longAxis), &(data->r))<0){ printf("Error in fscanf.\n");}
	    data->bType = bType;
	    data->xDomain = xDomain;
	    data->yDomain = yDomain;
	    data->zDomain = zDomain;
	    AddShapeFunction(SF, &cylinder, isDomain, (void*)data);
	  }
	  else if(!strcmp("ellipse", chtmp)){
	    struct ellipse_data *abc;
      PetscNew(&abc);
	    if(fscanf(f1, "%lf %lf %lf %lf %lf %lf", &(abc->a), &(abc->b), &(abc->c), &(abc->xc), &(abc->yc), &(abc->zc))<0){ printf("Error in fscanf.\n");}
	    AddShapeFunction(SF, &ellipse, isDomain, (void*)abc);
	  }
	  else if(!strcmp("rectangle", chtmp)){
	    struct rectangle_data *box;
      PetscNew(&box);
	    if(fscanf(f1,"%lf %lf %lf %lf %lf %lf", &(box->Bx), &(box->Ex), &(box->By), &(box->Ey), &(box->Bz), &(box->Ez))<0){ printf("Error in fscanf.\n");}
	    AddShapeFunction(SF, &rectangle, isDomain, (void*)box);
	  }
	}
	else if(!strcmp("xboundary", chtmp)){
	  if(fscanf(f1, "%d %d", &bType[0*2+0], &bType[0*2+1])<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp("yboundary", chtmp)){
	  if(fscanf(f1, "%d %d", &bType[1*2+0], &bType[1*2+1])<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp("zboundary", chtmp)){
	  if(fscanf(f1, "%d %d", &bType[2*2+0], &bType[2*2+1])<0){ printf("Error in fscanf.\n");}
	}
	else if(!strcmp("showData", chtmp)){
	  if(fscanf(f1, "%d", showData)<0){ printf("Error in fscanf.\n");}
	}
      }

    }

    fclose(f1);



}

static PetscErrorCode SaveLocalCoordinates(const char name[], DM forest)
{
  PetscViewer     viewer;
  Vec             coordinates;
  PetscMPIInt     rank;
  char            fname[PETSC_MAX_PATH_LEN];
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject) forest), &rank);CHKERRMPI(ierr);
  ierr = DMGetCoordinatesLocal(forest, &coordinates);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname, PETSC_MAX_PATH_LEN, "%s_%d.bin", name, rank);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_SELF, fname, FILE_MODE_WRITE, &viewer);CHKERRQ(ierr);
  ierr = VecView(coordinates, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SaveGlobalCoordinates(const char name[], DM forest)
{
  PetscViewer    viewer;
  Vec            coordinates;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetCoordinates(forest, &coordinates);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname, PETSC_MAX_PATH_LEN, "%s.bin", name);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, fname, FILE_MODE_WRITE, &viewer);CHKERRQ(ierr);
  ierr = VecView(coordinates, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode SaveGrid(const char name[], DM forest)
{
  DM             dm;
  PetscViewer    viewer;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* Convert Forest back to Plex for visualization */
  ierr = DMConvert(forest, DMPLEX, &dm);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname, PETSC_MAX_PATH_LEN, "%s.vtk", name);CHKERRQ(ierr);
  ierr = PetscViewerVTKOpen(PETSC_COMM_WORLD, fname, FILE_MODE_WRITE, &viewer);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode GridSetupSection(DM forest, int overlap)
{
  DM             plex;
  PetscSection   s;
  PetscInt       vStart, vEnd, v;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMConvert(forest, DMPLEX, &plex);CHKERRQ(ierr);
  ierr = DMPlexGetDepthStratum(plex, 0, &vStart, &vEnd);CHKERRQ(ierr);
  ierr = PetscSectionCreate(PetscObjectComm((PetscObject) forest), &s);CHKERRQ(ierr);
  ierr = PetscSectionSetChart(s, vStart, vEnd);CHKERRQ(ierr);
  for(v = vStart; v < vEnd; ++v) {ierr = PetscSectionSetDof(s, v, 1);CHKERRQ(ierr);}
  ierr = PetscSectionSetUp(s);CHKERRQ(ierr);
  ierr = DMSetLocalSection(forest, s);CHKERRQ(ierr);
  ierr = PetscSectionDestroy(&s);CHKERRQ(ierr);
  ierr = DMDestroy(&plex);CHKERRQ(ierr);

  ierr = DMGetGlobalSection(forest, &s);CHKERRQ(ierr);  // Make sure global section is setup.

  PetscFunctionReturn(0);
}

static PetscErrorCode GridSetupIndexLists(DM forest, ISCtx *idx)
{
  DM                      plex;
  PetscInt                vStart, vEnd, v, gStart;
  PetscErrorCode          ierr; 

  PetscFunctionBeginUser;
  ierr = DMConvert(forest, DMPLEX, &plex);CHKERRQ(ierr);
  ierr = DMPlexGetDepthStratum(plex, 0, &vStart, &vEnd);CHKERRQ(ierr);

  ierr = PetscMalloc1(vEnd-vStart, &idx->ghostIdx);CHKERRQ(ierr);
  ierr = PetscMalloc1(vEnd-vStart, &idx->localIdx);CHKERRQ(ierr);

  idx->szGhost = 0;
  idx->szLocal = 0;

  for(v=vStart;v<vEnd;++v){
    ierr = DMPlexGetPointGlobal(plex, v, &gStart, NULL);CHKERRQ(ierr);
    
    if(gStart<0){
      // Ghost vertex
      ierr = DMPlexGetPointLocal(plex, v, &idx->ghostIdx[idx->szGhost], NULL);CHKERRQ(ierr);
      ++idx->szGhost;
    }
    else{
      // Local vertex
      ierr = DMPlexGetPointLocal(plex, v, &idx->localIdx[idx->szLocal], NULL);CHKERRQ(ierr);
      ++idx->szLocal;
    }
  }
  ierr = PetscRealloc(sizeof(PetscInt*)*(idx->szGhost), &idx->ghostIdx);CHKERRQ(ierr);
  ierr = PetscRealloc(sizeof(PetscInt*)*(idx->szLocal), &idx->localIdx);CHKERRQ(ierr);

  ierr = DMDestroy(&plex);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode SaveRanks(const char name[], DM forest, ISCtx idx)
{
  PetscErrorCode  ierr;
  Vec             g, l;
  int             rank, v;
  PetscViewer     viewer;
  char            fname[PETSC_MAX_PATH_LEN];

  PetscFunctionBeginUser;

  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);

  // Set a local vector to store the rank owning all of the vertices
  ierr = DMCreateLocalVector(forest,&l);CHKERRQ(ierr);
  ierr = VecZeroEntries(l);CHKERRQ(ierr);

  for(v=0;v<idx.szLocal;++v){
    ierr = VecSetValue(l, idx.localIdx[v], rank, ADD_VALUES);CHKERRQ(ierr);
  }

  ierr = VecAssemblyBegin(l);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(l);CHKERRQ(ierr);

  // Now push the ranks to the global vector.
  ierr = DMCreateGlobalVector(forest,&g);CHKERRQ(ierr);
  ierr = VecZeroEntries(g);CHKERRQ(ierr);
  ierr = DMLocalToGlobal(forest, l, INSERT_VALUES, g);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname, PETSC_MAX_PATH_LEN, "%s.bin", name);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, fname, FILE_MODE_WRITE, &viewer);CHKERRQ(ierr);
  ierr = VecView(g, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  // Push it back to the local vector.
  ierr = VecZeroEntries(l);CHKERRQ(ierr);
  ierr = DMGlobalToLocal(forest, g, INSERT_VALUES, l);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname, PETSC_MAX_PATH_LEN, "%s_%d.bin", name, rank);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_SELF, fname, FILE_MODE_WRITE, &viewer);CHKERRQ(ierr);
  ierr = VecView(l, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  VecDestroy(&g);
  VecDestroy(&l);

  PetscFunctionReturn(0);
}

static PetscErrorCode SaveLevelSetVec(DM plex, ISCtx idx, PetscScalar ls[], const char name[], PetscViewer viewer)
{
  Vec             phiLocal, phi;
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;

  ierr = DMCreateLocalVector(plex, &phiLocal);CHKERRQ(ierr);
  ierr = VecZeroEntries(phiLocal);CHKERRQ(ierr);

  ierr = VecSetValues(phiLocal, idx.szLocal, idx.localIdx, ls, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(phiLocal);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phiLocal);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(plex, &phi);CHKERRQ(ierr);
  ierr = VecZeroEntries(phi);CHKERRQ(ierr);
  ierr = DMLocalToGlobal(plex, phiLocal, INSERT_VALUES, phi);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) phi, name);CHKERRQ(ierr);
  ierr = VecView(phi, viewer);CHKERRQ(ierr);

  ierr = VecDestroy(&phiLocal);CHKERRQ(ierr);
  ierr = VecDestroy(&phi);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode HDFPushGridLevelSet(DM forest, ShapeFunctions SF, ISCtx idx, PetscViewer viewer)
{
  DM              plex;
  Vec             coordinates;
  PetscInt        i, j, dim, lid = 0;
  PetscInt        szX[idx.szLocal], szY[idx.szLocal], szZ[idx.szLocal];
  PetscScalar     x[idx.szLocal], y[idx.szLocal], z[idx.szLocal], S;
  PetscScalar     ls0[idx.szLocal], lsX[idx.szLocal], lsY[idx.szLocal], lsZ[idx.szLocal];
  PetscErrorCode  ierr;
  
  PetscFunctionBeginUser;

  /* Create grid level set values arrays */
  ierr = DMGetDimension(forest, &dim);CHKERRQ(ierr); 
  ierr = DMGetCoordinatesLocal(forest, &coordinates);CHKERRQ(ierr);

  for(i=0;i<idx.szLocal;++i){
    szX[i] = dim*idx.localIdx[i];
    szY[i] = dim*idx.localIdx[i]+1;
  }

  ierr = VecGetValues(coordinates, idx.szLocal, szX, x);CHKERRQ(ierr);
  ierr = VecGetValues(coordinates, idx.szLocal, szY, y);CHKERRQ(ierr);

  if (dim == 2) {
    for(i=0;i<idx.szLocal;++i){
      z[i] = 0.0;
    }
  }
  else{
    for(i=0;i<idx.szLocal;++i){
      szZ[i] = dim*idx.localIdx[i]+2;
    }

    ierr = VecGetValues(coordinates, idx.szLocal, szZ, z);CHKERRQ(ierr);
  }
  
  for(i=0;i<idx.szLocal;++i){			
    for(j=0;j<SF.n;++j){				
	    S = SF.shape_func[j](x[i], y[i], z[i], 0, 0, 0, dim, SF.ctx[j]);	
      if(S<=0.0){								
        ls0[i] = SF.shape_func[j](x[i], y[i], z[i], 0, 0, 0, dim, SF.ctx[j]);
        lsX[i] = SF.shape_func[j](x[i], y[i], z[i], 1, 0, 0, dim, SF.ctx[j]);
        lsY[i] = SF.shape_func[j](x[i], y[i], z[i], 0, 1, 0, dim, SF.ctx[j]);
        lsZ[i] = SF.shape_func[j](x[i], y[i], z[i], 0, 0, 1, dim, SF.ctx[j]);
      }
    }
  }

  /* Convert Forest to Plex for now to get the vectors*/
  ierr = DMConvert(forest, DMPLEX, &plex);CHKERRQ(ierr);

  ierr = PetscViewerHDF5PushGroup(viewer, "/GridLevelSet");CHKERRQ(ierr);
  
  ierr = SaveLevelSetVec(plex, idx, ls0, "phi", viewer);CHKERRQ(ierr);
  ierr = SaveLevelSetVec(plex, idx, lsX, "phiX", viewer);CHKERRQ(ierr);
  ierr = SaveLevelSetVec(plex, idx, lsY, "phiY", viewer);CHKERRQ(ierr);
  ierr = SaveLevelSetVec(plex, idx, lsZ, "phiZ", viewer);CHKERRQ(ierr);

  ierr = PetscViewerHDF5PopGroup(viewer);CHKERRQ(ierr);

  ierr = DMDestroy(&plex);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

static PetscErrorCode HDFPushFileVersion(PetscViewer viewer)
{
  Vec             major, minor;
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  ierr = PetscViewerHDF5PushGroup(viewer, "/FileVersion");CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&major);CHKERRQ(ierr);
  ierr = VecSetSizes(major,PETSC_DECIDE,1);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) major, "major");CHKERRQ(ierr);
  ierr = VecSetFromOptions(major);CHKERRQ(ierr);
  ierr = VecSetValue(major,0, (PetscInt)PROGRAM_GRID_FILE_VERSION_MAJOR, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(major);VecAssemblyEnd(major);CHKERRQ(ierr);
  ierr = VecView(major, viewer);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&minor);CHKERRQ(ierr);
  ierr = VecSetSizes(minor,PETSC_DECIDE,1);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) minor, "minor");CHKERRQ(ierr);
  ierr = VecSetFromOptions(minor);CHKERRQ(ierr);
  ierr = VecSetValue(minor,0, (PetscInt)PROGRAM_GRID_FILE_VERSION_MINOR, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(minor);VecAssemblyEnd(minor);CHKERRQ(ierr);
  ierr = VecView(minor, viewer);CHKERRQ(ierr);

  ierr = PetscViewerHDF5PopGroup(viewer);CHKERRQ(ierr);
  ierr = VecDestroy(&major);CHKERRQ(ierr);
  ierr = VecDestroy(&minor);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

static PetscErrorCode HDFPushBoundaryType(PetscViewer viewer, int *bType)
{
  PetscInt        hdfbType[3][2] = {{bType[0*2+0], bType[0*2+1]}, {bType[1*2+0], bType[1*2+1]}, {bType[2*2+0], bType[2*2+1]}};
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  
  ierr = PetscViewerHDF5PushGroup(viewer, "/BoundaryType");CHKERRQ(ierr);
  ierr = PetscViewerHDF5WriteAttribute(viewer, "x-boundary", "left", PETSC_INT, &hdfbType[0][0]);CHKERRQ(ierr);
  ierr = PetscViewerHDF5WriteAttribute(viewer, "x-boundary", "right", PETSC_INT, &hdfbType[0][1]);CHKERRQ(ierr);
  ierr = PetscViewerHDF5WriteAttribute(viewer, "y-boundary", "left", PETSC_INT, &hdfbType[1][0]);CHKERRQ(ierr);
  ierr = PetscViewerHDF5WriteAttribute(viewer, "y-boundary", "right", PETSC_INT, &hdfbType[1][1]);CHKERRQ(ierr);
  ierr = PetscViewerHDF5WriteAttribute(viewer, "z-boundary", "left", PETSC_INT, &hdfbType[2][0]);CHKERRQ(ierr);
  ierr = PetscViewerHDF5WriteAttribute(viewer, "z-boundary", "right", PETSC_INT, &hdfbType[2][1]);CHKERRQ(ierr);
  ierr = PetscViewerHDF5PopGroup(viewer);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*	A note about min_refine and max_refine. When a DM is created in petsc it's already at a refine level of 1.*/
/* To run: ./grid -i sample.input*/
int main (int argc, char **argv)
{

  DM             base, forest;
  PetscBool      interpolate = PETSC_TRUE;
  PetscErrorCode ierr;

  /*	The maximum extent of the domain*/
  double xDomain[2] = {0, 0};
  double yDomain[2] = {0, 0};
  double zDomain[2] = {0, 0};

  /*	Periodicity*/
  int bType[6] = {STANDARD, STANDARD, STANDARD, STANDARD, STANDARD, STANDARD};	/*	Default to all standard*/

  /*	Number of grid point in a direction*/
  int min_refine = 0;
  int max_refine = 0;
  int dim = 2;

  /*	Name of file to save to*/
  char fname[255] = "NO_FILE_WAS_EVER_SET_SO_THIS_IS_THE_DEFAULT_OUTPUT.h5";

  int showData = 1;
  int nDomains = 0;

  ShapeFunctions SF;
  MPI_Comm       comm;
  PetscMPIInt    size, rank;
  char           filename[PETSC_MAX_PATH_LEN];
  PetscBool      flg;

  const char *boundaryTypeText[3];
  boundaryTypeText[STANDARD] = "Std";
  boundaryTypeText[OUTFLOW] = "Out";
  boundaryTypeText[PERIODIC] = "Per";

  /*	Initialize MPI*/
  ierr = PetscInitialize(&argc, &argv, NULL, NULL);if (ierr) return ierr;

  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_size(comm, &size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr);

  ierr = PetscOptionsGetString(NULL, NULL, "-i", filename, PETSC_MAX_PATH_LEN, &flg);CHKERRQ(ierr);
  if (!flg) {
    ierr = PetscPrintf(comm, "Usage: grid -i data_file.\n\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "The input file has the following format (where all caps are to be entered by the user):\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "grid_file NAME_TO_USE\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "refinements MIN MAX\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "dimensions DIM\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "domain_size BEGIN_X END_X BEGIN_Y END_Y BEGIN_Z END_Z\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "domain SHAPE_TYPE SHAPE_PARAMETERS\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "shape SHAPE_TYPE SHAPE_PARAMETERS\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "xboundary LEFT RIGHT\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "yboundary BOTTOM TOP\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "zboundary BACK FRONT\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "Possible shapes are given by the shape and then parameters:\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "sphere RADIUS CENTRE_X CENTRE_Y CENTRE_Z\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "hollow_sphere INN_RAD OUT_RAD CENTRE_X CENTRE_Y CENTRE_Z\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "ellipse RAD_X RAD_Y RAD_Z CENTRE_X CENTRE_Y CENTRE_Z\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "rectangle BX EX BY EY BZ EZ\n");CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "cylinder LONG_AXIS[0(x)/1(y)/2(z)] RADIUS\n");CHKERRQ(ierr);
    exit(0);
  }



  /*	By default assume that the entire domain is used.*/
  CreateShapeFunctionStructure(&SF);

  parse_file(filename, fname, &min_refine, &max_refine, &dim, xDomain, yDomain, zDomain, bType, &showData, &nDomains, &SF);

  if (!nDomains) {
    if (showData) {ierr = PetscPrintf(comm, "Assuming the entire grid is used. Setting domain function to rectangle.\n");CHKERRQ(ierr);}

    struct rectangle_data *box;
    PetscNew(&box);

    box->Bx = xDomain[0];
    box->Ex = xDomain[1];
    box->By = yDomain[0];
    box->Ey = yDomain[1];
    box->Bz = zDomain[0];
    box->Ez = zDomain[1];

    AddShapeFunction(&SF, &rectangle, TRUE, (void*)box);
  }


  /*	If either of the directions are periodic then set the other to also be periodic*/
  if(bType[0*2+0]==PERIODIC || bType[0*2+1]==PERIODIC) bType[0*2+0] = bType[0*2+1] = PERIODIC;
  if(bType[1*2+0]==PERIODIC || bType[1*2+1]==PERIODIC) bType[1*2+0] = bType[1*2+1] = PERIODIC;
  if(bType[2*2+0]==PERIODIC || bType[2*2+1]==PERIODIC) bType[2*2+0] = bType[2*2+1] = PERIODIC;

  if (showData) {
    ierr = PetscPrintf(comm, "Constructing a grid file with the following parameters:\n");
    ierr = PetscPrintf(comm, "%50s: %s\n", "File Name", fname);
    ierr = PetscPrintf(comm, "%50s: %+.2f\t%+.2f\n", "x-direction bounds",xDomain[0],xDomain[1]);
    ierr = PetscPrintf(comm, "%50s: %+.2f\t%+.2f\n", "y-direction bounds",yDomain[0],yDomain[1]);
    ierr = PetscPrintf(comm, "%50s: %+.2f\t%+.2f\n", "z-direction bounds",zDomain[0],zDomain[1]);
    ierr = PetscPrintf(comm, "%50s: %d\n", "Minimum Refinement", min_refine);
    ierr = PetscPrintf(comm, "%50s: %d\n", "Maximum Refinement", max_refine);
    ierr = PetscPrintf(comm, "%50s: %d\n", "Dimension", dim);
    ierr = PetscPrintf(comm, "%50s: %s\t%s\n", "x-boundary", boundaryTypeText[bType[0*2+0]], boundaryTypeText[bType[0*2+1]]);
    ierr = PetscPrintf(comm, "%50s: %s\t%s\n", "y-boundary", boundaryTypeText[bType[1*2+0]], boundaryTypeText[bType[1*2+1]]);
    ierr = PetscPrintf(comm, "%50s: %s\t%s\n", "z-boundary", boundaryTypeText[bType[2*2+0]], boundaryTypeText[bType[2*2+1]]);

    ierr = PetscPrintf(comm, "\n");

    for (int i = 0; i < SF.n; ++i){

      if(SF.isDomain[i]==TRUE) ierr = PetscPrintf(comm, "%50s: ", "Domain Shape Function");
      else ierr = PetscPrintf(comm, "%50s: ", "Embedded Shape Function");

      if(SF.shape_func[i]==&sphere){
        ierr = PetscPrintf(comm, "%s\n", "Sphere");
        struct sphere_data circle = *((struct sphere_data*)SF.ctx[i]);
        ierr = PetscPrintf(comm, "%50s: %.2f\n", "Sphere Radius", circle.R);
        ierr = PetscPrintf(comm, "%50s: %.2f\t%.2f\t%.2f\n", "Sphere Centre", circle.xc, circle.yc, circle.zc);
      }
      else if(SF.shape_func[i]==&hollow_sphere){
        ierr = PetscPrintf(comm, "%s\n", "Hollow Sphere");
        struct hollow_sphere_data r = *((struct hollow_sphere_data*)SF.ctx[i]);
        ierr = PetscPrintf(comm, "%50s: %.2f\t%.2f\n", "Hollow Sphere Parameters", r.r0, r.r1);
        ierr = PetscPrintf(comm, "%50s: %.2f\t%.2f\t%.2f\n", "Hollow Sphere Centre", r.xc, r.yc, r.zc);
      }
      else if(SF.shape_func[i]==&cylinder){
        ierr = PetscPrintf(comm, "%s\n", "Cylinder");
        struct cylinder_data data = *((struct cylinder_data*)SF.ctx[i]);
        PetscPrintf(PETSC_COMM_WORLD,"%50s: %d\t%.2f\n", "Cylinder Parameters", data.longAxis, data.r);
      }
      else if(SF.shape_func[i]==&ellipse){
        ierr = PetscPrintf(comm, "%s\n", "Ellipse");
        struct ellipse_data abc = *((struct ellipse_data*)SF.ctx[i]);
        ierr = PetscPrintf(comm, "%50s: %.2f\t%.2f\t%.2f\n", "Ellipsoid Parameters", abc.a,abc.b,abc.c);
        ierr = PetscPrintf(comm, "%50s: %.2f\t%.2f\t%.2f\n", "Ellipsoid Centre", abc.xc, abc.yc, abc.zc);
      }
      else if(SF.shape_func[i]==&rectangle){
        ierr = PetscPrintf(comm, "%s\n", "Rectangle");
        struct rectangle_data box = *((struct rectangle_data*)SF.ctx[i]);
        ierr = PetscPrintf(comm, "%50s: %.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", "Rectangle Parameters", box.Bx,box.Ex,box.By,box.Ey,box.Bz,box.Ez);
      }
      ierr = PetscPrintf(comm, "\n");
    }
  }

  // If any of the domain shape functions have the following:
  //  1) are a rectangle
  //	2) have edges which match the embedding space
  //	3) are periodic on the edges
  //	then adjust the bounds to allow for an accurate SDF determination.
  for (int i = 0; i < SF.n; ++i){
    if(SF.isDomain[i]==TRUE && SF.shape_func[i]==&rectangle){
      struct rectangle_data *box = (struct rectangle_data *)SF.ctx[i];
      if(bType[0*2+0]==PERIODIC && (fabs(box->Bx-xDomain[0])<1e-8 || fabs(box->Ex-xDomain[1])<1e-8)){
        box->Bx = -1e8;
        box->Ex = +1e8;
      }
      if(bType[1*2+0]==PERIODIC && (fabs(box->By-yDomain[0])<1e-8 || fabs(box->Ey-yDomain[1])<1e-8)){
        box->By = -1e8;
        box->Ey = +1e8;
      }
      if(bType[2*2+0]==PERIODIC && (fabs(box->Bz-zDomain[0])<1e-8 || fabs(box->Ez-zDomain[1])<1e-8)){
        box->Bz = -1e8;
        box->Ez = +1e8;
      }
    }

  }

  PetscReal      lower[dim], upper[dim];

  if (dim==2){
    lower[0] = xDomain[0]; lower[1] = yDomain[0];
    upper[0] = xDomain[1]; upper[1] = yDomain[1];
  }
  else{
    lower[0] = xDomain[0]; lower[1] = yDomain[0]; lower[2] = zDomain[0];
    upper[0] = xDomain[1]; upper[1] = yDomain[1]; upper[2] = zDomain[1];
  }


  /* Create a base DMPlex mesh */
  PetscInt faces[3] = {1,1,1};
  PetscInt overlap = 0;
  ierr = PetscOptionsGetInt(NULL, NULL, "-overlap", &overlap, &flg);CHKERRQ(ierr);
  if(flg){ PetscPrintf(PETSC_COMM_WORLD, "Setting overlap to %d\n", overlap); }
  ierr = DMPlexCreateBoxMesh(PETSC_COMM_WORLD, dim, PETSC_FALSE, faces, lower, upper, NULL, interpolate, &base);CHKERRQ(ierr);
  ierr = DMSetFromOptions(base);CHKERRQ(ierr);
  ierr = DMGetDimension(base,&dim);CHKERRQ(ierr);
  /* Covert Plex mesh to Forest and destroy base */
  ierr = DMCreate(PETSC_COMM_WORLD, &forest);CHKERRQ(ierr);
  ierr = DMSetType(forest, (dim == 2) ? DMP4EST : DMP8EST);CHKERRQ(ierr);
  ierr = DMForestSetBaseDM(forest, base);CHKERRQ(ierr);
  ierr = DMForestSetInitialRefinement(forest, min_refine);CHKERRQ(ierr);
  ierr = DMForestSetPartitionOverlap(forest, overlap);CHKERRQ(ierr);
  ierr = DMSetUp(forest);CHKERRQ(ierr);
  ierr = DMDestroy(&base);CHKERRQ(ierr);

  /* Refinement Loop */
  for (int n = min_refine;n>0;--n){
    DM rdm;

    ierr = AdaptGrid(forest, CoarsenDomain, SF, &rdm);CHKERRQ(ierr);
    ierr = DMDestroy(&forest);CHKERRQ(ierr);
    forest = rdm;
  }
#if 1
  for (int n = min_refine; n < max_refine; ++n) {
    DM rdm;

    ierr = AdaptGrid(forest, RefineInterface, SF, &rdm);CHKERRQ(ierr);
    ierr = DMDestroy(&forest);CHKERRQ(ierr);
    forest = rdm;
  }
#endif

  ierr = SaveGrid("sol", forest);CHKERRQ(ierr);
  ierr = SaveLocalCoordinates("coord", forest);CHKERRQ(ierr);
  ierr = SaveGlobalCoordinates("coords", forest);CHKERRQ(ierr);

  ierr = GridSetupSection(forest, overlap);CHKERRQ(ierr);
  
  /* Test the vectors*/
  Vec      l, g;
  PetscInt lsz, gsz;
  ISCtx    idx;

  ierr = DMSetApplicationContext(forest, &idx);CHKERRQ(ierr);

  ierr = DMViewFromOptions(forest, NULL, "-dm_view");CHKERRQ(ierr);
  ierr = DMCreateLocalVector(forest,&l);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(forest,&g);CHKERRQ(ierr);

  ierr = VecZeroEntries(l);CHKERRQ(ierr);
#if 1
  ierr = DMLocalToGlobal(forest, l, INSERT_VALUES, g);CHKERRQ(ierr);
  ierr = DMGlobalToLocal(forest, g, INSERT_VALUES, l);CHKERRQ(ierr);
#endif

  ierr = VecGetSize(l, &lsz);CHKERRQ(ierr);
  ierr = VecGetSize(g, &gsz);CHKERRQ(ierr);

  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD, "%d: %d\t%d\n", rank, lsz, gsz);CHKERRQ(ierr);
  ierr = PetscSynchronizedFlush(PETSC_COMM_WORLD, PETSC_STDOUT);CHKERRQ(ierr);

  ierr = GridSetupIndexLists(forest, &idx);CHKERRQ(ierr);
  ierr = SaveRanks("ranks", forest, idx);CHKERRQ(ierr);

  /* HDF Viewer */
  PetscViewer    viewer;

  ierr = PetscViewerCreate(PETSC_COMM_WORLD, &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer, PETSCVIEWERHDF5);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer, fname);CHKERRQ(ierr);

  /* File Version */
  ierr = HDFPushFileVersion(viewer);CHKERRQ(ierr);

  /* Boundary Type */  
  ierr = HDFPushBoundaryType(viewer, bType);CHKERRQ(ierr);

  /* Level Set Values */
  ierr = HDFPushGridLevelSet(forest, SF, idx, viewer);CHKERRQ(ierr);

  ierr = DMView(forest, viewer);CHKERRQ(ierr);

  /* Cleanup */
  ierr = VecDestroy(&l);CHKERRQ(ierr);
  ierr = VecDestroy(&g);CHKERRQ(ierr);
  ierr = DMDestroy(&forest);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = PetscFree(idx.ghostIdx);CHKERRQ(ierr);
  ierr = PetscFree(idx.localIdx);CHKERRQ(ierr);
  ierr = PetscFree(SF.shape_func);CHKERRQ(ierr);
  ierr = PetscFree(SF.ctx);CHKERRQ(ierr);
  ierr = PetscFree(SF.isDomain);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;

}


