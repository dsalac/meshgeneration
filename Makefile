 # --- Makefile ---

SOURCES.c = PetscGridSet.c shapes.c
OBJECTS   = $(SOURCES.c:.c=.o)

.PHONY: clean

grid: ${OBJECTS}
	${CLINKER} -o $@ $^ ${PETSC_LIB}

clean::
	rm -f grid ${OBJECTS} *.bin *.bin.info

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules