#define STANDARD 0
#define OUTFLOW 1
#define PERIODIC 2

#define FMAX(A,B) ( (A) > (B) ? (A):(B)) 
#define FMIN(A,B) ( (A) < (B) ? (A):(B))

#include <mpi.h>
#include <unistd.h>
#include "hdf5.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "shapes.h"

#define ELLIPSE 0
#define SPHERE 1
#define RECTANGLE 2

#define FALSE 0
#define TRUE 1

#define EPS 1E-12

double DMIN(double A, double B){	if(A<B) return(A); 	else return(B);	}
double DMAX(double A, double B){	if(A>B) return(A); 	else return(B);	}
double DSQR(double A){ return(A*A); }

double sphere(const double xin, const double yin, const double zin,  const int dx, const int dy, const int dz, const int dim, void *ctx){
	
  struct sphere_data *data = (struct sphere_data *)ctx;
  double x = xin - data->xc;
  double y = yin - data->yc;
  double z;
  if (dim==3) z = zin - data->zc;
  else z = 0.0;
  double a = data->R;

  
  if(dx==0 && dy==0 && dz==0)		
    return(sqrt(x*x+y*y+z*z)-a);
  else if(dx==1 && dy==0 && dz==0)
    return(x/(sqrt(x*x+y*y+z*z)+1E-12));
  else if(dx==0 && dy==1 && dz==0)	
    return(y/(sqrt(x*x+y*y+z*z)+1E-12));			
  else if(dx==0 && dy==0 && dz==1)
    return(z/(sqrt(x*x+y*y+z*z)+1E-12));			
  else
    return(1E10);
  
}

double hollow_sphere(const double xin, const double yin, const double zin,  const int dx, const int dy, const int dz, const int dim, void *ctx){
	
  struct hollow_sphere_data *data = (struct hollow_sphere_data *)ctx;
  double x = xin - data->xc;
  double y = yin - data->yc;
  double z;
  if (dim==3) z = zin - data->zc;
  else z = 0.0;
  double r0 = data->r0;
  double r1 = data->r1;
  
  double rh = 0.5*(r0+r1);
  double r = sqrt(x*x+y*y+z*z);
  
  if(r<rh){	/*	Use the inner sphere*/
    if(dx==0 && dy==0 && dz==0)		
      return(r0-r);
    else if(dx==1 && dy==0 && dz==0)
      return(-x/(sqrt(x*x+y*y+z*z)+1E-12));
    else if(dx==0 && dy==1 && dz==0)	
      return(-y/(sqrt(x*x+y*y+z*z)+1E-12));			
    else if(dx==0 && dy==0 && dz==1)
      return(-z/(sqrt(x*x+y*y+z*z)+1E-12));			
    else
      return(1E10);
  }
  else{		/*	Use the outer sphere*/
    if(dx==0 && dy==0 && dz==0)		
      return(r-r1);
    else if(dx==1 && dy==0 && dz==0)
      return(x/(sqrt(x*x+y*y+z*z)+1E-12));
    else if(dx==0 && dy==1 && dz==0)	
      return(y/(sqrt(x*x+y*y+z*z)+1E-12));			
    else if(dx==0 && dy==0 && dz==1)
      return(z/(sqrt(x*x+y*y+z*z)+1E-12));			
    else
      return(1E10);
  }
  
  
  return(1E10);
  
}

double ellipse(const double xin, const double yin, const double zin,  const int dx, const int dy, const int dz, const int dim, void *ctx){
	
  struct ellipse_data *data = (struct ellipse_data *)ctx;
  double a = data->a;
  double b = data->b;

  double x = xin - data->xc;
  double y = yin - data->yc;
  double c, z;
  
  if (dim==3) {
    z = zin - data->zc;
    c = data->c;
  }
  else {
    z = 0.0;
    c = 1.0;
  }
  
  if(dx==0 && dy==0 && dz==0)		
    return(sqrt((x/a)*(x/a)+(y/b)*(y/b)+(z/c)*(z/c))- 1.0);
  else if(dx==1 && dy==0 && dz==0)
    return(x/(a*a*sqrt(x*x+y*y+z*z)+1E-12));			
  else if(dx==0 && dy==1 && dz==0)	
    return(y/(b*b*sqrt(x*x+y*y+z*z)+1E-12));			
  else if(dx==0 && dy==0 && dz==1)
    return(z/(c*c*sqrt(x*x+y*y+z*z)+1E-12));			
  else
    return(1E10);
}

double cylinder(const double x, const double y, const double z,  const int dx, const int dy, const int dz, const int dim, void *ctx){
  
  struct cylinder_data *data = (struct cylinder_data *)ctx;
  short longAxis = data->longAxis;
  double r0 = data->r;
  int *bType = data->bType;
  double *xDomain = data->xDomain;
  double *yDomain = data->yDomain;
  double *zDomain = data->zDomain;
  double r;
  double val = 1E10;
  
  if(fabs(x-xDomain[0])<1.0E-8 && bType[0*2+0]==STANDARD){
    if(dx==0 && dy==0 && dz==0) val = 1.0;
    else if(dx==1 && dy==0 && dz==0) val = -1.0;
    else val = 0.0;
  }
  else if(fabs(x-xDomain[1])<1.0E-8 && bType[0*2+1]==STANDARD){
    if(dx==0 && dy==0 && dz==0) val = 1.0;
    else if(dx==1 && dy==0 && dz==0) val = 1.0;
    else val = 0.0;
  }
  else if(fabs(y-yDomain[0])<1.0E-8 && bType[1*2+0]==STANDARD){
    if(dx==0 && dy==0 && dz==0) val = 1.0;
    else if(dx==0 && dy==1 && dz==0) val = -1.0;
    else val = 0.0;
  }
  else if(fabs(y-yDomain[1])<1.0E-8 && bType[1*2+1]==STANDARD){
    if(dx==0 && dy==0 && dz==0) val = 1.0;
    else if(dx==0 && dy==1 && dz==0) val = 1.0;
    else val = 0.0;	
  }
  else if(fabs(z-zDomain[0])<1.0E-8 && bType[2*2+0]==STANDARD){
    if(dx==0 && dy==0 && dz==0) val = 1.0;
    else if(dx==0 && dy==0 && dz==1) val = -1.0;
    else val = 0.0;
  }
  else if(fabs(z-zDomain[1])<1.0E-8 && bType[2*2+1]==STANDARD){
    if(dx==0 && dy==0 && dz==0) val = 1.0;
    else if(dx==0 && dy==0 && dz==1) val = 1.0;
    else val = 0.0;
  }
  else {
    
    switch(longAxis){
    case 0:		//	x-axis
      r = sqrt(y*y + z*z);
      if(dx==0 && dy==0 && dz==0)		
	val = r-r0;
      else if(dx==1 && dy==0 && dz==0)
	val = 0.0;
      else if(dx==0 && dy==1 && dz==0)	
	val = y/(r+1.0E-12);
      else if(dx==0 && dy==0 && dz==1)
	val = z/(r+1.0E-12);
      else
	val = 1E10;
      break;
    case 1: // y-axis
      r = sqrt(x*x + z*z);
      if(dx==0 && dy==0 && dz==0)		
	val = r-r0;
      else if(dx==1 && dy==0 && dz==0)
	val = x/(r+1.0E-12);
      else if(dx==0 && dy==1 && dz==0)	
	val = 0.0;
      else if(dx==0 && dy==0 && dz==1)
	val = z/(r+1.0E-12);
      else
	val = 1E10;
      break;
    case 2: // z-axis
      r = sqrt(x*x + y*y);
      if(dx==0 && dy==0 && dz==0)		
	val = r-r0;
      else if(dx==1 && dy==0 && dz==0)
	val = x/(r+1.0E-12);
      else if(dx==0 && dy==1 && dz==0)	
	val = y/(r+1.0E-12);
      else if(dx==0 && dy==0 && dz==1)
	val = 0.0;
      else
	val = 1E10;
      break;
    };
  }
  
  return(val);
  
}

double rectangle2D(const double x, const double y, const double z, const int dx, const int dy, const int dz, void *ctx){

  struct rectangle_data *data = (struct rectangle_data *)ctx;
  double Bx = data->Bx;
  double Ex = data->Ex;
  double By = data->By;
  double Ey = data->Ey;	
	
	
  int i, imin;
  double d, dmin;
  double n[2], g;

  double X = FMAX(FMIN(x, Ex), Bx);
  double Y = FMAX(FMIN(y, Ey), By);	
	

  /*	All of the possible closest points*/
  double points[8][2] = {
			 {Bx,  Y},	/*	Rectangle edge*/
			 {Ex,  Y},	/*	Rectangle edge*/
			 { X, By},	/*	Rectangle edge*/
			 { X, Ey},	/*	Rectangle edge*/								
			 {Bx, By},	/*	Rectangle corner*/
			 {Bx, Ey},	/*	Rectangle corner*/								
			 {Ex, By},	/*	Rectangle corner*/
			 {Ex, Ey}};	/*	Rectangle corner*/									
	
  dmin = 1E10;
	
  /*	Compute the minimum distance*/
  for(i=0;i<8;++i){
    d = sqrt(DSQR(x - points[i][0]) + DSQR(y - points[i][1]));
    if(d<dmin){
      imin = i;
      dmin = d;
    }
  }

  /*	The normal vector*/
  n[0] = x - points[imin][0];
  n[1] = y - points[imin][1];
	
  g = sqrt(n[0]*n[0]+n[1]*n[1]);
	
  g = FMAX(g, EPS);

  if(x-Bx>-EPS && x-Ex<EPS && y-By>-EPS && y-Ey<EPS){
    dmin *= -1.0;
    n[0] *= -1.0;
    n[1] *= -1.0;		
  }
	
  if(dx==1 && dy==0 && dz==0)
    return(n[0]/g);
  else if(dx==0 && dy==1 && dz==0)
    return(n[1]/g);
  else if(dx==0 && dy==0 && dz==1)
    return(0.0);
	
  return(dmin);
	
}


double rectangle3D(const double x, const double y, const double z, const int dx, const int dy, const int dz, void *ctx){
	
  struct rectangle_data *data = (struct rectangle_data *)ctx;
  double Bx = data->Bx;
  double Ex = data->Ex;
  double By = data->By;
  double Ey = data->Ey;
  double Bz = data->Bz;
  double Ez = data->Ez;
	
	
  int i, imin;
  double d, dmin;
  double n[3], g;

  double X = FMAX(FMIN(x, Ex), Bx);
  double Y = FMAX(FMIN(y, Ey), By);
  double Z = FMAX(FMIN(z, Ez), Bz);
	

  /*	All of the possible closest points*/
  double points[26][3] = {
			  {Bx,  Y,  Z},	/*	Rectangle face*/
			  {Ex,  Y,  Z},	/*	Rectangle face*/
			  { X, By,  Z},	/*	Rectangle face*/
			  { X, Ey,  Z},	/*	Rectangle face*/
			  { X,  Y, Bz},	/*	Rectangle face*/
			  { X,  Y, Ez},	/*	Rectangle face*/
			  {Bx, By,  Z},	/*	Rectangle edge*/
			  {Bx, Ey,  Z},	/*	Rectangle edge*/
			  {Bx,  Y, Bz},	/*	Rectangle edge*/
			  {Bx,  Y, Ez},	/*	Rectangle edge*/
			  {Ex, By,  Z},	/*	Rectangle edge*/
			  {Ex, Ey,  Z},	/*	Rectangle edge*/
			  {Ex,  Y, Bz},	/*	Rectangle edge*/
			  {Ex,  Y, Ez},	/*	Rectangle edge*/
			  { X, By, Bz},	/*	Rectangle edge*/
			  { X, By, Ez},	/*	Rectangle edge*/
			  { X, Ey, Bz},	/*	Rectangle edge*/
			  { X, Ey, Ez},	/*	Rectangle edge*/
			  {Bx, Ey, Ez},	/*	Rectangle corner*/
			  {Bx, By, Ez},	/*	Rectangle corner*/
			  {Bx, Ey, Bz},	/*	Rectangle corner*/
			  {Bx, By, Bz},	/*	Rectangle corner*/
			  {Ex, By, Ez},	/*	Rectangle corner*/
			  {Ex, Ey, Ez},	/*	Rectangle corner*/
			  {Ex, By, Bz},	/*	Rectangle corner*/
			  {Ex, By, Ez}};	/*	Rectangle corner*/
	
  dmin = 1E10;
	
  /*	Compute the minimum distance*/
  for(i=0;i<26;++i){
    d = sqrt(DSQR(x - points[i][0]) + DSQR(y - points[i][1]) + DSQR(z - points[i][2]));
    if(d<dmin){
      imin = i;
      dmin = d;
    }
  }
	
  /*	The normal vector*/
  n[0] = x - points[imin][0];
  n[1] = y - points[imin][1];
  n[2] = z - points[imin][2];
  g = sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
	
  g = FMAX(g, EPS);

  if(x-Bx>-EPS && x-Ex<EPS && y-By>-EPS && y-Ey<EPS && z-Bz>-EPS && z-Ez<EPS){
    dmin *= -1.0;
    n[0] *= -1.0;
    n[1] *= -1.0;
    n[2] *= -1.0;
  }
	
  if(dx==1 && dy==0 && dz==0)
    return(n[0]/g);
  else if(dx==0 && dy==1 && dz==0)
    return(n[1]/g);
  else if(dx==0 && dy==0 && dz==1)
    return(n[2]/g);	
	

	
  return(dmin);
  
  
}

double rectangle(const double x, const double y, const double z, const int dx, const int dy, const int dz, const int dim, void *ctx){

  double val;
  switch(dim){
    case 2:
      val = rectangle2D(x, y, z, dx, dy, dz, ctx);
      break;
    case 3:
      val = rectangle3D(x, y, z, dx, dy, dz, ctx);
      break;
    default:
      printf("Simulations can be either 2D or 3D.\n");
      exit(0);
  }
  return(val);
}

