
struct sphere_data{
  double R;
  double xc;
  double yc;
  double zc;
};

struct hollow_sphere_data{
  double r0;
  double r1;
  double xc;
  double yc;
  double zc;
};

struct ellipse_data{
  double a;
  double b;
  double c;
  double xc;
  double yc;
  double zc;
};

struct cylinder_data{
  int longAxis;		// 0: x, 1: y, 2: z
  double r;		// Radius
  int *bType;		// Type of boundary
  double *xDomain;	// x-range
  double *yDomain;	// y-range
  double *zDomain;	// z-range
};

struct rectangle_data{
  double Bx;
  double Ex;
  double By;
  double Ey;
  double Bz;
  double Ez;
  int bType[6];
};

double sphere(const double xin, const double yin, const double zin,  const int dx, const int dy, const int dz, const int Nz, void *ctx);
double hollow_sphere(const double xin, const double yin, const double zin,  const int dx, const int dy, const int dz, const int Nz, void *ctx);
double ellipse(const double xin, const double yin, const double zin,  const int dx, const int dy, const int dz, const int Nz, void *ctx);
double cylinder(const double x, const double y, const double z,  const int dx, const int dy, const int dz, const int Nz, void *ctx);
double rectangle2D(const double x, const double y, const double z, const int dx, const int dy, const int dz, void *ctx);
double rectangle3D(const double x, const double y, const double z, const int dx, const int dy, const int dz, void *ctx);
double rectangle(const double x, const double y, const double z, const int dx, const int dy, const int dz, const int Nz, void *ctx);

